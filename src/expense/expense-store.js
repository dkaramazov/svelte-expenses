import { writable } from 'svelte/store';

const expenseList = writable([]);

export default {
    subscribe: expenseList.subscribe,
    setExpenses: expenses => expenseList.set(expenses),
    saveExpense: expense => expenseList.update(expenses => expenses.concat(expense)),
    removeExpense: id => expenseList.update(expenses => expenses.filter(x => x.id != id)),
    editExpense: edited => expenseList.update(expenses => {
        Object.assign(expenses.find(x => x.id == edited.id), edited);
        return expenses;
    }),
}

async function saveExpense() {

}

async function getExpenses() {

}

async function editExpense() {

}

async function removeExpense() {
    
}