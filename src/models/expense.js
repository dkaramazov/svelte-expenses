export default function Expense({id = null, fields = null, createdTime = getDate()} = {}) {
    this.amount = fields && fields.Amount || 0;
    this.budget = fields && fields.Budget || '';
    this.date = fields && fields.Date || getDate();
    this.expenseType = fields && fields.ExpenseType || '';
    this.name = fields && fields.Name || '';
    this.id = id;
    this.createdTime = createdTime;

    async function save() {
        if(this.id) {
            // new
        } else {
            // edit
        }   
    }

    async function remove() {

    }
}

function getDate() {
    return new Date().toLocaleString('en-US', { timeZone: 'UTC' }).split(',')[0];
}